<%@ include file="cabecera.jsp"%>
<%
	String us;
	if (session.getAttribute("usuario") != null)
		us = session.getAttribute("usuario").toString();
	else
		us = "Invitado";
	//String us = "Alexander";
%>

<script>
	function mostrarCuestionario(id) {
		$.get("../ShowQuestionnarie", {
			id : id
		}, function(data) {
			$("#contenido").html(data);
		});
	}
	
	function connect() {
    	var target = "ws://localhost:8080/PW_Cuestionario/Procesar_Mensajes";
        if ('WebSocket' in window) {
            ws = new WebSocket(target);
        } else if ('MozWebSocket' in window) {
            ws = new MozWebSocket(target);
        } else {
            alert('WebSocket is not supported by this browser.');
            return;
        }
        ws.onopen = function () {
            //setConnected(true);
        	 var message =  $("#usuario").val();
             ws.send(message);
        };
        ws.onmessage = function (event) {
            enlinea(event.data);
        };
        ws.onclose = function (event) {
            //setConnected(false);
            //log('Info: WebSocket connection closed, Code: ' + event.code + (event.reason == "" ? "" : ", Reason: " + event.reason));
        };
    }
	
	function echo() {
        if (ws != null) {
            var message =  $("#usuario").val();
            alert(message);
            ws.send(message);
        } else {
            alert('WebSocket connection not established, please connect.');
        }
    }
	
	function enlinea(usu) {
		$( "#useronline" ).append( '<div class="usuarioconectado">' + usu + '</div>');
	}

	$(document).ready(function() {
		$("#cuestionario").submit(function(e) {
			e.defaultPrevented();
		});

		$("#btn_mostrar").click(function() {
			/*if ($("#btn_mostrar").val() == "Mostrar") {
				$("#miscuestionarios").show("slow");
				$("#btn_mostrar").val("Ocultar");
			} else {
				$("#miscuestionarios").hide("slow");
				$("#btn_mostrar").val("Mostrar");
			}*/
			$("#miscuestionarios").toggle();
		});

		var user = $("#usuario").val();
		alert("Bienvenido usuario: " + user);
		
		$.get("../ShowQuestionnairiesList", {
			user : user
		}, function(data) {
			$("#miscuestionarios").html(data);
		});

		$("#calificar").click(function() {
			var p1 = $("#p1").val();
			var p2 = $("#p2").val();
			var p3 = $("#p3").val();
			var p4 = $("#p4").val();
			var p5 = $("#p5").val();
			var p6 = $("#p6").val();
			var p7 = $("#p7").val();
			var p8 = $("#p8").val();
			var p9 = $("#p9").val();
			var p10 = $("#p10").val();

			$.get("../Calificacion", {
				p1 : p1,
				p2 : p2,
				p3 : p3,
				p4 : p4,
				p5 : p5,
				p6 : p6,
				p7 : p7,
				p8 : p8,
				p9 : p9,
				p10 : p10
			}, function(data) {
				alert(data);
			});
		});
		//funci�n para iniciar el websocket.
		connect();
		
	});
</script>

<div
	style="text-align: right; border-bottom: medium; border-bottom-color: red; border-bottom-style: solid;">
	<input id="usuario" type="text" value="<%out.print(us);%>" />
</div>
<div>
	<div  id="useronline">
		<b>Usuarios en l�nea:</b>
		<!-- <div class="usuarioconectado">	Leyla	</div>	<div class="usuarioconectado">	Alex </div> -->
	</div>
	<div id="cuerpocuestionarios">
<input type="button" id="btn_mostrar" value="Mostrar" />
<div id="miscuestionarios"
	style="text-align: left; margin-left: 80px; display: none;"></div>

<div id="contenido" style="text-align: left; margin-left: 80px;"></div>
<div style="text-align: left; margin-left: 80px;">
	<input type="button" id="calificar" value="Calificar" />
</div>
	</div>
	
</div>	

<%@ include file="pie.jsp"%>