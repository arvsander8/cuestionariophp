<?php include_once("cabecera.php"); ?>

<style>
	#indice{
		width: 800px;
		margin-left: 80px;
		margin-right: 50px;
		text-align: justify;
		
	}
</style>

<div id="indice">
	<h2> Easy Survey</h2>
	<h3> Cuestionarios Online</h3>
	<p>EasySurvey es una herramienta online que le permitirá crear sus
		cuestionarios en linea, y compartirlos con sus amigos, alumnos y/o
		publicamente.</p>
	<p>Busque los cuestionarios públicos creados por otras personas y
		respondalos, el sistema le gererará automáticamente su nota, la que
		quedará registrada en su historial de cuestionarios realizados, para
		luego ver sus progresos.</p>
	<p>Comparta los cuestionarios con sus alumnos para tomar exámenes
		online, acceda a su cuenta para indicar que el cuestionario es de una
		sola resolución y no puede ser modificada. Asi tendrá un exámen
		practico y sencillo para sus alumnos, del mismo modo, escoja la
		cantidad de preguntas que apreceran a los alumnos y escoja las
		opciones de preguntas aleatorias para que aleatorice, la posicion, el
		orden de las preguntas y alternativas, asi como las preguntas que van
		a aperecer.</p>
</div>

<?php include_once("pie.php");?>