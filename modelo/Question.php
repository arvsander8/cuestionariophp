<?php
class Question{

	private $id;
	private $code;
	private $question;
	private $comments;
	private $state;
	private $iddifficulty;
	private $alternativecorrect;
	private $idquestionnaire;
	//agregamos un campo más.. que es el arraylist.
	private $alternativas;

	public function Question($id, $code, $question, $comments,
			$state, $iddifficulty, $alternativecorrect,
			$idquestionnaire) {
		$this->id = $id;
		$this->code = $code;
		$this->question = $question;
		$this->comments = $comments;
		$this->state = $state;
		$this->iddifficulty = $iddifficulty;
		$this->alternativecorrect = $alternativecorrect;
		$this->idquestionnaire = $idquestionnaire;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode($code) {
		$this->code = $code;
	}

	public function getQuestion() {
		return $this->question;
	}

	public function setQuestion($question) {
		$this->question = $question;
	}

	public function getComments() {
		return $this->comments;
	}

	public function setComments($comments) {
		$this->comments = $comments;
	}

	public function getState() {
		return $this->state;
	}

	public function setState($state) {
		$this->state = $state;
	}

	public function getIddifficulty() {
		return $this->iddifficulty;
	}

	public function setIddifficulty($iddifficulty) {
		$this->iddifficulty = $iddifficulty;
	}

	public function getAlternativecorrect() {
		return $this->alternativecorrect;
	}

	public function setAlternativecorrect($alternativecorrect) {
		$this->alternativecorrect = $alternativecorrect;
	}

	public function getIdquestionnaire() {
		return $this->idquestionnaire;
	}

	public function setIdquestionnaire($idquestionnaire) {
		$this->idquestionnaire = $idquestionnaire;
	}
	
	public function getAlternativas() {
		return $this->alternativas;
	}

	public function setAlternativas($alternativas) {
		$this->alternativas = $alternativas;
	}

}

