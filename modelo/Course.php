<?php
class Course{

	private $id;
	private $idenrolment;
	private $description;
	private $credits;
	private $capacity;

	public function __construct($id, $idenrolment, $description,
			$credits, $capacity) {
		$this->id = $id;
		$this->idenrolment = $idenrolment;
		$this->description = $description;
		$this->credits = $credits;
		$this->capacity = $capacity;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getIdenrolment() {
		return $this->idenrolment;
	}

	public function setIdenrolment($idenrolment) {
		$this->idenrolment = $idenrolment;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getCredits() {
		return $this->credits;
	}

	public function setCredits($credits) {
		$this->credits = $credits;
	}

	public function getCapacity() {
		return $this->capacity;
	}

	public function setCapacity($capacity) {
		$this->capacity = $capacity;
	}

}
