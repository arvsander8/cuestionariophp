<?php class Topic{

	private $id;
	private $idcourse;
	private $description;

	public function __construct($id, $idcourse, $description) {
		$this->id = $id;
		$this->idcourse = $idcourse;
		$this->description = $description;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getIdcourse() {
		return $this->idcourse;
	}

	public function setIdcourse($idcourse) {
		$this->idcourse = $idcourse;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

}
