<?php 
class Questionnaire{

	private $id;
	private $idtopic;
	private $title;
	private $type;

	public function __construct($id, $idtopic, $title, $type) {
		$this->id = $id;
		$this->idtopic = $idtopic;
		$this->title = $title;
		$this->type = $type;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getIdtopic() {
		return $this->idtopic;
	}

	public function setIdtopic($idtopic) {
		$this->idtopic = $idtopic;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function getType() {
		return $this->type;
	}

	public function setType($type) {
		$this->type = $type;
	}

}
