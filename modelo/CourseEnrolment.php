<?php
class CourseEnrolment {

	private $id;
	private $idCourse;
	private $idEnrolment;

	
	public function CourseEnrolment($id, $idCourse, $idEnrolment) {
		$this->id = $id;
		$this->idCourse = $idCourse;
		$this->idEnrolment = $idEnrolment;
	}

	public function getId() {
		return $this->id;
	}

	public function  setId($id) {
		$this->id = $id;
	}

	public function getIdCourse() {
		return $this->idCourse;
	}

	public function  setIdCourse($idCourse) {
		$this->idCourse = $idCourse;
	}

	public function getIdEnrolment() {
		return $this->idEnrolment;
	}

	public function  setIdEnrolment($idEnrolment) {
		$this->idEnrolment = $idEnrolment;
	}

}
