<?php

class Conexion {

    //put your code here

    private $link = null;
    
    
    
    public function conectar($host, $port, $bd, $user, $pass) {
        $str = "host=$host port=$port dbname=$bd user=$user password=$pass";
        $this->link = pg_Connect($str) or die("No se pudo conectar a la BD".  pg_last_error());
        
    }
    

    public function desconectar() {
        pg_close($this->link);
    }

    public function get_link() {
        return $this->link;
    }

    public function set_link($host, $port, $bd, $user, $pass) {
        $this->conectar($host, $port, $bd, $user, $pass);
    }

    public function setDefault() {
        $this->conectar('localhost', '5432', 'cuestionario', 'postgres', 'sander7l');
    }

    public function getSiguienteId($tabla){
        $sql = "select max(id) as m from $tabla limit 1";
        $result = pg_query($this->link, $sql);
        $row = pg_fetch_array($result);
        pg_free_result($result);
        return $row['m'] +1;
    }
}

//$con = new Conexion();
//$tst = $con->setDefault();