<?php
class Alternative{

	private $id;
	private $description;
	private $idquestion;

	public function __construct($id){
            $this->id = $id;
	}

	

	public function Alternative($id, $description, $idquestion) {
		$this->id = $id;
		$this->description = $description;
		$this->idquestion = $idquestion;
	}

	public function getId() {
		return $this.id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getIdquestion() {
		return $this.idquestion;
	}

	public function setIdquestion($idquestion) {
		$this->idquestion = $idquestion;
	}

}
