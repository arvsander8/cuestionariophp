<?php
class Enrolment{

	private $id;
	private $iduser;
	private $code;
	private $dateenrolment;
	private $timeenrolment;
	private $type;

	public function __construct($id, $iduser, $code, $dateenrolment,
			$timeenrolment, $type) {
		$this->id = $id;
		$this->iduser = $iduser;
		$this->code = $code;
		$this->dateenrolment = $dateenrolment;
		$this->timeenrolment = $timeenrolment;
		$this->type = $type;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getIduser() {
		return $this->iduser;
	}

	public function setIduser($iduser) {
		$this->iduser = $iduser;
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode($code) {
		$this->code = $code;
	}

	public function getDateenrolment() {
		return $this->dateenrolment;
	}

	public function setDateenrolment($dateenrolment) {
		$this->dateenrolment = $dateenrolment;
	}

	public function getTimeenrolment() {
		return $this->timeenrolment;
	}

	public function setTimeenrolment($timeenrolment) {
		$this->timeenrolment = $timeenrolment;
	}

	public function getType() {
		return $this->type;
	}

	public function setType($type) {
		$this->type = $type;
	}

}
